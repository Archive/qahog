/* ipod-method.c - VFS modules for the iPod
 *
 *  Copyright (C) 2001,2002,2003 Bastien Nocera
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Author: Bastien Nocera <hadess@hadess.net>
 * Inspired by Alex Larsson's neato SMB vfs method
 */

#include "config.h"

/* libgen first for basename */
#include <libgen.h>
#include <string.h>
#include <glib.h>

#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime.h>

#include <libgnomevfs/gnome-vfs-method.h>
#include <libgnomevfs/gnome-vfs-module.h>
#include <libgnomevfs/gnome-vfs-module-shared.h>
#include <libgnomevfs/gnome-vfs-module-callback-module-api.h>
#include <libgnomevfs/gnome-vfs-standard-callbacks.h>

#include <gconf/gconf-client.h>

#include <iPod.h>

#define DEFAULT_MOUNT_PATH "/mnt/ipod"
#define GCONF_MOUNT_PATH "/apps/qahog/mount_path"

/* Global vars: */
static GMutex *ipod_lock = NULL;

static itunesdb_t *itunesdb = NULL;
static GList *songs = NULL, *playlists = NULL;
static gboolean dirty = TRUE;
static GHashTable *songs_table = NULL, *playlists_table = NULL;
static char *mount_path = NULL;

#define DEBUG_IPOD_ENABLE
//#define DEBUG_IPOD_LOCKS

#ifdef DEBUG_IPOD_ENABLE
#define DEBUG_IPOD(x) g_warning x
#else
#define DEBUG_IPOD(x) 
#endif

#ifdef DEBUG_IPOD_LOCKS
#define LOCK_IPOD() 	{g_warning ("TRY LOCK %s\n", __FUNCTION__); g_mutex_lock (ipod_lock); g_warning ("LOCK %s\n", __FUNCTION__);}
#define UNLOCK_IPOD() 	{g_warning ("UNLOCK %s\n", __FUNCTION__); g_mutex_unlock (ipod_lock); g_warning ("UNLOCKED %s\n", __FUNCTION__);}
#else
#define LOCK_IPOD() 	g_mutex_lock (ipod_lock)
#define UNLOCK_IPOD() 	g_mutex_unlock (ipod_lock)
#endif

static gboolean
_gnome_vfs_uri_has_grand_parent (GnomeVFSURI *uri)
{
	GnomeVFSURI *parent;
	gboolean retval;

	if (gnome_vfs_uri_has_parent (uri) == FALSE)
		return FALSE;

	parent = gnome_vfs_uri_get_parent (uri);
	retval = gnome_vfs_uri_has_parent (parent);
	gnome_vfs_uri_unref (parent);

	return retval;
}

static char *
_gnome_vfs_uri_get_basename (const GnomeVFSURI *uri)
{
	char *path = g_strdup (uri->text);
	char *base;

	DEBUG_IPOD(("_gnome_vfs_uri_get_basename: uri: %s, basename: %s\n",
				uri->text, basename (uri->text)));

	base = g_strdup (basename (path));
	g_free (path);

	return base;
}

static char *
get_mount_path (GConfClient *gc)
{
	char *path;

	path = gconf_client_get_string (gc, GCONF_MOUNT_PATH, NULL);
	if (path == NULL || strcmp (path, "") == 0)
		return g_strdup (DEFAULT_MOUNT_PATH);
	else
		return path;
}

static void
mount_path_changed_cb (GConfClient *client, guint cnxn_id,
		GConfEntry *entry, gpointer user_data)
{
	g_free (mount_path);
	mount_path = NULL;
	mount_path = get_mount_path (client);
	LOCK_IPOD ();
	dirty = TRUE;
	UNLOCK_IPOD();
}

static char *
get_itunesdb_path (void)
{
	return g_build_filename (G_DIR_SEPARATOR_S, mount_path,
			"iPod_Control/iTunes/iTunesDB", NULL);
}

static void
empty_database (void)
{
	db_free(itunesdb);
	db_song_list_free (songs);
	db_playlist_list_free (playlists);
	if (songs_table != NULL)
		g_hash_table_destroy (songs_table);
	if (playlists_table != NULL)
		g_hash_table_destroy (playlists_table);
	itunesdb = NULL;
	songs = NULL;
	playlists = NULL;
	songs_table = NULL;
	playlists_table = NULL;
	dirty = FALSE;
}

/* We need to be locked to use this function */
static GnomeVFSResult
update_database (void)
{
	char *path, *realpath;
	GList *l;
	tihm_t *tihm;
	pyhm_t *pyhm;
	int *id;

	if (dirty == TRUE)
		empty_database ();

	if (itunesdb != NULL)
		return GNOME_VFS_OK;

	//FIXME check and create the directory structure if necessary
	//mkdir -p iPod_Control/iTunes/

	path = get_itunesdb_path ();
	if (path == NULL)
	{
		itunesdb = NULL;
		return GNOME_VFS_ERROR_IO;
	}

	itunesdb = g_new0 (itunesdb_t, 1);

	if (db_load (itunesdb, path) < 0)
	{
		if (g_file_test (path, G_FILE_TEST_IS_REGULAR) == FALSE)
		{
			//create_database (path);
			//FIXME before bailing out, try a db_create()
		}

		//FIXME before bailing out, try a db_create()
		g_free (path);
		g_free (itunesdb);
		itunesdb = NULL;
		return GNOME_VFS_ERROR_IO;
	}
	g_free (path);

	if (db_sanity_check(itunesdb) <= -20)
	{
		db_free(itunesdb);
		itunesdb = NULL;
		return GNOME_VFS_ERROR_BAD_FILE;
	}

	/* Songs */
	songs = db_song_list (itunesdb);
	songs_table = g_hash_table_new (g_str_hash, g_str_equal);
	for (l = songs; l != NULL; l = l->next)
	{
		tihm = l->data;

		realpath = iPodGetSongRealPathFromMount (mount_path, tihm);
		g_hash_table_insert (songs_table,
				g_strdup (basename (realpath)), realpath);
	}

	/* Playlists */
	playlists = db_playlist_list (itunesdb);
	playlists_table = g_hash_table_new (g_int_hash, g_int_equal);
	for (l = playlists; l != NULL; l = l->next)
	{
		pyhm = l->data;

		if (pyhm->num != 0)
		{
			id = g_new (int, 1);
			*id = pyhm->num;
			g_hash_table_insert (playlists_table, id, pyhm->name);
		}
	}

	return GNOME_VFS_OK;
}

/* We need to be locked to use this function */
static char *
find_path_for_file (const GnomeVFSURI *uri, GnomeVFSResult *ret_result)
{
	GnomeVFSResult res;
	char *realpath, *file, *basefile;

	realpath = NULL;

	res = update_database ();

	if (res != GNOME_VFS_OK)
	{
		(*ret_result) = res;
		return NULL;
	}

	DEBUG_IPOD(("find_path_for_file (): uri: %s",
				gnome_vfs_uri_to_string (uri, 0)));

	file = gnome_vfs_unescape_string (uri->text, "/");
	basefile = basename (file);

	DEBUG_IPOD(("find_path_for_file (): looking up %s", basefile));
	realpath = (char *) g_hash_table_lookup (songs_table,
			(gpointer) basefile);
	g_free (file);

	if (realpath == NULL)
	{
		(*ret_result) = GNOME_VFS_ERROR_NOT_FOUND;
		return NULL;
	}

	return g_strdup (realpath);
}
//FIXME
#if 0
static gboolean
file_is_directory (const GnomeVFSURI *uri)
{
	return FALSE;
}
#endif
typedef struct {
	GnomeVFSHandle *handle;
	gboolean read_only;
	char *target_path;
} FileHandle;

static GnomeVFSResult
do_open (GnomeVFSMethod *method,
	 GnomeVFSMethodHandle **method_handle,
	 GnomeVFSURI *uri,
	 GnomeVFSOpenMode mode,
	 GnomeVFSContext *context)
{
	GnomeVFSResult res;
	GnomeVFSHandle *fh = NULL;
	FileHandle *handle = NULL;
	char *path;
	gboolean read_only = TRUE;

	DEBUG_IPOD(("do_open() %s mode %d",
				gnome_vfs_uri_to_string (uri, 0), mode));

	if (!(mode & GNOME_VFS_OPEN_READ) && !(mode & GNOME_VFS_OPEN_WRITE))
		return GNOME_VFS_ERROR_INVALID_OPEN_MODE;

	LOCK_IPOD ();
	path = find_path_for_file (uri, &res);
	UNLOCK_IPOD ();

	if (path == NULL && !(mode & GNOME_VFS_OPEN_WRITE))
	{
		g_message ("WTF");
		if (res != GNOME_VFS_OK)
			return res;
		else
			return GNOME_VFS_ERROR_NOT_FOUND;
	} else if (path != NULL && (mode & GNOME_VFS_OPEN_WRITE)) {
		g_free (path);
		return GNOME_VFS_ERROR_ACCESS_DENIED;
	} else if (path == NULL && (mode & GNOME_VFS_OPEN_WRITE)) {
		char *filename;

		filename = _gnome_vfs_uri_get_basename (uri);
		path = iPodMakePathName (mount_path, filename);
		g_free (filename);
		read_only = FALSE;
	}

	DEBUG_IPOD(("do_open() %s -> %s",
				gnome_vfs_uri_to_string (uri, 0), path));

	if (read_only == TRUE)
	{
		res = gnome_vfs_open (&fh, path, mode);
	} else {
		res = gnome_vfs_create (&fh, path, mode, TRUE, 0666);
	}

	if (res != GNOME_VFS_OK)
		return res;

	handle = g_new0 (FileHandle, 1);
	handle->handle = fh;
	handle->read_only = read_only;
	handle->target_path = gnome_vfs_unescape_string (path, "/");
	g_free (path);

	*method_handle = (GnomeVFSMethodHandle *)handle;

	return res;
}

static GnomeVFSResult
do_close (GnomeVFSMethod *method,
	  GnomeVFSMethodHandle *method_handle,
	  GnomeVFSContext *context)

{
	FileHandle *handle = (FileHandle *)method_handle;
	GnomeVFSResult res;

	DEBUG_IPOD(("do_close()"));

	res = gnome_vfs_close (handle->handle);

	if (res == GNOME_VFS_OK)
	{
		char * dbpath;
		tihm_t * tihm;

		tihm = getMP3Info (handle->target_path);
		if (tihm == NULL)
		{
			DEBUG_IPOD(("do_close(): can't get mp3 info for %s",
						handle->target_path));
			res = GNOME_VFS_ERROR_WRONG_FORMAT;
			goto bail;
		}

		addMP3PathInfo (mount_path, tihm, handle->target_path);
		LOCK_IPOD ();
		if (db_add (itunesdb, tihm) != 0)
		{
			UNLOCK_IPOD();
			DEBUG_IPOD(("do_close(): can't add to the database"));
			res = GNOME_VFS_ERROR_WRONG_FORMAT;
			goto bail;
		}
		dbpath = get_itunesdb_path ();
		g_message ("dbpath: %s", dbpath);
		if (db_write( *(itunesdb), dbpath ) < 0)
		{
			UNLOCK_IPOD();
			DEBUG_IPOD(("do_close(): can't write the database"));
			g_free (dbpath);
			res = GNOME_VFS_ERROR_INTERNAL;
			goto bail;
		}

		/* We managed to upload the song, mark the database as dirty */
		dirty = TRUE;
		UNLOCK_IPOD();
		g_free (dbpath);
	}

bail:
	g_free (handle);

	return res;
}

static GnomeVFSResult
do_read (GnomeVFSMethod *method,
	 GnomeVFSMethodHandle *method_handle,
	 gpointer buffer,
	 GnomeVFSFileSize num_bytes,
	 GnomeVFSFileSize *bytes_read,
	 GnomeVFSContext *context)
{
	FileHandle *handle = (FileHandle *)method_handle;

	DEBUG_IPOD(("do_read() %Lu bytes", num_bytes));

	return gnome_vfs_read (handle->handle, buffer, num_bytes, bytes_read);
}

static GnomeVFSResult
do_write (GnomeVFSMethod *method,
	  GnomeVFSMethodHandle *method_handle,
	  gconstpointer buffer,
	  GnomeVFSFileSize num_bytes,
	  GnomeVFSFileSize *bytes_written,
	  GnomeVFSContext *context)


{
	GnomeVFSResult res;
	FileHandle *handle = (FileHandle *)method_handle;

	DEBUG_IPOD (("do_write() %p", method_handle));

	res = gnome_vfs_write (handle->handle, buffer, num_bytes,
			bytes_written);

	return res;
}

static GnomeVFSResult
do_create (GnomeVFSMethod *method,
	   GnomeVFSMethodHandle **method_handle,
	   GnomeVFSURI *uri,
	   GnomeVFSOpenMode mode,
	   gboolean exclusive,
	   guint perm,
	   GnomeVFSContext *context)

{
	DEBUG_IPOD (("do_create() %s mode %d",
				gnome_vfs_uri_to_string (uri, 0), mode));

	return do_open (method, method_handle, uri, GNOME_VFS_OPEN_WRITE,
			context);
}

static GnomeVFSResult
do_get_file_info (GnomeVFSMethod *method,
		  GnomeVFSURI *uri,
		  GnomeVFSFileInfo *file_info,
		  GnomeVFSFileInfoOptions options,
		  GnomeVFSContext *context)

{
	GnomeVFSResult res;
	char *path;

	DEBUG_IPOD (("do_get_file_info() %s",
				gnome_vfs_uri_to_string (uri, 0)));

	/* Check if it's the root */
	if (gnome_vfs_uri_has_parent (uri) == FALSE)
	{
		file_info->name = g_strdup ("/");
		file_info->valid_fields = GNOME_VFS_FILE_INFO_FIELDS_TYPE |
			GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;
		file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
		file_info->mime_type = g_strdup ("x-directory/normal");
										                return GNOME_VFS_OK;
	}

	LOCK_IPOD ();
	path = find_path_for_file (uri, &res);
	UNLOCK_IPOD ();

	if (path == NULL)
	{
		if (res != GNOME_VFS_OK)
			return res;
		else
			return GNOME_VFS_ERROR_NOT_FOUND;
	}

	res = gnome_vfs_get_file_info (path, file_info, options);
	g_free (path);

	/* Lie if it's not found, it's in the database if path != NULL */
	if (res == GNOME_VFS_ERROR_NOT_FOUND)
		res = GNOME_VFS_OK;

	return res;
}

static GnomeVFSResult
do_get_file_info_from_handle (GnomeVFSMethod *method,
		GnomeVFSMethodHandle *method_handle,
		GnomeVFSFileInfo *file_info,
		GnomeVFSFileInfoOptions options,
		GnomeVFSContext *context)
{
	FileHandle *handle = (FileHandle *)method_handle;

	DEBUG_IPOD(("do_get_file_info_from_handle"));

	return gnome_vfs_get_file_info_from_handle (handle->handle,
			file_info, options);
}

static gboolean
do_is_local (GnomeVFSMethod *method,
	     const GnomeVFSURI *uri)
{
	DEBUG_IPOD (("do_is_local(): %s", gnome_vfs_uri_to_string (uri,
					GNOME_VFS_URI_HIDE_NONE)));

	return FALSE;
}

typedef struct {
	GList *posl;
	int song_len;
	int pl_len;
	guint pos;
} DirectoryHandle;

static GnomeVFSResult
do_open_directory (GnomeVFSMethod *method,
		   GnomeVFSMethodHandle **method_handle,
		   GnomeVFSURI *uri,
		   GnomeVFSFileInfoOptions options,
		   GnomeVFSContext *context)

{
	GnomeVFSResult res;
	DirectoryHandle *directory_handle;

	DEBUG_IPOD(("do_open_directory() %s",
		gnome_vfs_uri_to_string (uri, 0)));

	//FIXME check the uri

	LOCK_IPOD();
	res = update_database ();
	if (res != GNOME_VFS_OK)
	{
		UNLOCK_IPOD();
		return res;
	}

	/* Construct the handle */
	directory_handle = g_new0 (DirectoryHandle, 1);
	directory_handle->posl = songs;
	directory_handle->pos = 0;
	directory_handle->song_len = g_list_length (songs);
	/* We skip playlist 0, as it's the main playlist that we would show */
	directory_handle->pl_len = g_list_length (playlists) - 1;
	*method_handle = (GnomeVFSMethodHandle *) directory_handle;

	UNLOCK_IPOD();

	DEBUG_IPOD(("do_open_directory end"));
	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_close_directory (GnomeVFSMethod *method,
		    GnomeVFSMethodHandle *method_handle,
		    GnomeVFSContext *context)
{
	DirectoryHandle *directory_handle = (DirectoryHandle *) method_handle;

	DEBUG_IPOD(("do_close_directory: %p", directory_handle));

	if (directory_handle == NULL)
		return GNOME_VFS_OK;

	g_free (directory_handle);

	DEBUG_IPOD(("do_close_directory: end"));

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_read_directory (GnomeVFSMethod *method,
		   GnomeVFSMethodHandle *method_handle,
		   GnomeVFSFileInfo *file_info,
		   GnomeVFSContext *context)
{
	DirectoryHandle *dh = (DirectoryHandle *) method_handle;
	char *realpath;
	GList *list;
	tihm_t *tihm;

	DEBUG_IPOD (("do_read_directory()"));

	LOCK_IPOD();
	if (dh->pos > dh->song_len + dh->pl_len + 2)
	{
		UNLOCK_IPOD();
		return GNOME_VFS_ERROR_EOF;
	}

	/* . or .. directories */
	if (dh->pos == 0 || dh->pos == 1)
	{
		file_info->valid_fields = GNOME_VFS_FILE_INFO_FIELDS_NONE |
			GNOME_VFS_FILE_INFO_FIELDS_TYPE |
			GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;

		file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
		file_info->name = g_strdup ((dh->pos == 0) ? "." : "..");
		file_info->mime_type = g_strdup ("x-directory/normal");
		DEBUG_IPOD (("do_read_directory(): read folder %s",
					file_info->name));
		dh->pos++;
		UNLOCK_IPOD();
		return GNOME_VFS_OK;
	} else if (dh->pos < dh->pl_len + 2) {
		GList *l;
		pyhm_t *pyhm;

		/* The first element is the master playlist, we're not showing
		 * it as a subdirectory */
		l = g_list_nth (playlists, dh->pos - 2 + 1);
		g_assert (l != NULL);
		pyhm = (pyhm_t *)l->data;

		file_info->valid_fields = GNOME_VFS_FILE_INFO_FIELDS_NONE |
			GNOME_VFS_FILE_INFO_FIELDS_TYPE |
			GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;

		file_info->type = GNOME_VFS_FILE_TYPE_DIRECTORY;
		file_info->name = g_strdup (pyhm->name);
		file_info->mime_type = g_strdup ("x-directory/normal");
		DEBUG_IPOD (("do_read_directory(): read folder %s",
					file_info->name));
		dh->pos++;
		UNLOCK_IPOD();
		return GNOME_VFS_OK;
	}

	list = dh->posl;
	if (list == NULL)
	{
		UNLOCK_IPOD();
		return GNOME_VFS_ERROR_EOF;
	}

	GNOME_VFS_FILE_INFO_SET_LOCAL (file_info, TRUE);

	/* Tell gnome-vfs which fields will be valid */
	file_info->valid_fields = GNOME_VFS_FILE_INFO_FIELDS_NONE |
		GNOME_VFS_FILE_INFO_FIELDS_TYPE |
		GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE |
		GNOME_VFS_FILE_INFO_FIELDS_SIZE;

	tihm = dh->posl->data;

	/* Fill the fields */
	file_info->type = GNOME_VFS_FILE_TYPE_REGULAR;

	realpath = iPodGetSongRealPathFromMount (mount_path, tihm);
	file_info->name = g_strdup (basename (realpath));
	file_info->mime_type = g_strdup
		(gnome_vfs_mime_type_from_name (realpath));
	g_free (realpath);

	file_info->size = tihm->size;
	DEBUG_IPOD (("do_read_directory(): read file %s", file_info->name));

	dh->pos++;
	dh->posl = dh->posl->next;

	DEBUG_IPOD (("do_read_directory(): position %d", dh->pos));
	UNLOCK_IPOD();
	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_unlink (GnomeVFSMethod *method,
	   GnomeVFSURI *uri,
	   GnomeVFSContext *context)
{
	GList *l;
	char *file, *tmp, *dbpath;
	tihm_t *tihm;
	GnomeVFSResult res;

	DEBUG_IPOD (("do_unlink() %s",
				gnome_vfs_uri_to_string (uri, 0)));

	tihm = NULL;

	LOCK_IPOD();
	res = update_database ();
	if (res != GNOME_VFS_OK)
	{
		UNLOCK_IPOD();
		return res;
	}

	file = find_path_for_file (uri, &res);
	if (res != GNOME_VFS_OK)
	{
		UNLOCK_IPOD();
		return res;
	}

	for (l = songs; l != NULL; l = l->next)
	{
		tihm = l->data;
		tmp = iPodGetSongRealPathFromMount (mount_path, tihm);
		//DEBUG_IPOD (("do_unlink() comparing '%s' and '%s'", file, tmp));
		if (strcmp (tmp, file) == 0)
		{
			g_free (tmp);
			break;
		}
		g_free (tmp);
		tihm = NULL;
	}

	if (tihm == NULL) {
		UNLOCK_IPOD();
		return GNOME_VFS_ERROR_NOT_FOUND;
	}

	res = gnome_vfs_unlink (file);

	/* If we fail, but it's in the database, lie */
	if (res != GNOME_VFS_OK && res != GNOME_VFS_ERROR_NOT_FOUND)
	{
		g_message ("gnome_vfs_unlink failed %s", gnome_vfs_result_to_string (res));
		UNLOCK_IPOD();
		return res;
	}

	db_remove (itunesdb, tihm->num);
	dbpath = get_itunesdb_path ();
	if (db_write( *(itunesdb), dbpath ) < 0)
	{
		UNLOCK_IPOD();
		g_free (dbpath);
		g_free (file);
		return GNOME_VFS_ERROR_INTERNAL;
	}

	dirty = TRUE;
	UNLOCK_IPOD();
	g_free (file);

	return res;
}

static GnomeVFSResult
do_check_same_fs (GnomeVFSMethod *method,
		  GnomeVFSURI *a,
		  GnomeVFSURI *b,
		  gboolean *same_fs_return,
		  GnomeVFSContext *context)
{
	DEBUG_IPOD (("do_check_same_fs()"));

	if (strcmp (gnome_vfs_uri_get_scheme (a),
				gnome_vfs_uri_get_scheme (b)) == 0)
		*same_fs_return = TRUE;
	else
		*same_fs_return = FALSE;

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_move (GnomeVFSMethod *method,
	 GnomeVFSURI *old_uri,
	 GnomeVFSURI *new_uri,
	 gboolean force_replace,
	 GnomeVFSContext *context)
{
	DEBUG_IPOD (("do_move() %s %s",
				gnome_vfs_uri_to_string (old_uri, 0),
				gnome_vfs_uri_to_string (new_uri, 0)));

#if 0
  	GnomeVFSResult res;
	int old_card, old_folder, old_song;
	int new_card, new_folder, new_song;
	gboolean move_folder = FALSE;
	char *new_name;

	g_warninging("This function of the rio500 method is not properly supported\nPlease mail hadess@hadess.net with information about your program\n");

	DEBUG_IPOD (("do_move() %s %s\n",
		    gnome_vfs_uri_to_string (old_uri, 0),
		    gnome_vfs_uri_to_string (new_uri, 0)));
	LOCK_IPOD();

	res = lookup_uri(old_uri, &old_card, &old_folder, &old_song);
	DEBUG_IPOD(("do_move(): lookup (src) %d %d %d\n",
				old_card, old_folder, old_song));
	/* if the original file isn't found, bail */
	if (res != GNOME_VFS_OK)
	{
		UNLOCK_IPOD();
		return res;
	}

	res = lookup_uri(new_uri, &new_card, &new_folder, &new_song);
	DEBUG_IPOD(("do_move(): lookup (dest) %d %d %d\n",
				new_card, new_folder, new_song));

	/* if the dest file is found and force_replace is false, bail */
	if (res == GNOME_VFS_OK && force_replace == FALSE)
	{
		UNLOCK_IPOD();
		return GNOME_VFS_ERROR_FILE_EXISTS;
	} else {
		UNLOCK_IPOD();
		return GNOME_VFS_ERROR_NOT_SUPPORTED;
	}

	/* if the dest uri is invalid, bail */
	if (res == GNOME_VFS_ERROR_INVALID_URI)
	{
		UNLOCK_IPOD();
		return res;
	}

	/* if the move crosses disks, or tries to the rename the cards, bail */
	if (new_card != old_card || new_card == -1)
	{
		UNLOCK_IPOD();
		return GNOME_VFS_ERROR_NOT_SUPPORTED;
	}

	/* Moving songs */
	if (old_song != -1)
	{
		/* are they in the same folder ? */
		if (old_folder != new_folder)
		{
			UNLOCK_IPOD();
			return GNOME_VFS_ERROR_NOT_SUPPORTED;
		}
		move_folder = FALSE;
	}

	rio->rio_hw = rio_new();
	if (rio_check(rio->rio_hw) == FALSE)
	{
		DEBUG_IPOD(("do_move: check of rio is FALSE, destroying\n"));
		UNLOCK_IPOD();
		return GNOME_VFS_ERROR_IO;
	}
	rio_set_card(rio->rio_hw, old_card);

	/* Getting the name of the file/folder */
	{
		char *tmp;

		tmp = gnome_vfs_uri_get_basename(new_uri);
		if (tmp != NULL)
			new_name = tmp;
		else
			new_name = gnome_vfs_unescape_string(tmp, "/");
	}

	if (move_folder == TRUE)
	{
		res = rio_to_vfs_error(rio_rename_folder(rio->rio_hw,
					old_folder, (char *)new_name));
	} else {
		res = rio_to_vfs_error(rio_rename_song(rio->rio_hw,
					old_folder, old_song,
					(char *)new_name));
	}
	rio_delete(rio->rio_hw);

	if (res == GNOME_VFS_OK)
	{
		(get_content_from_card (old_card)->dirty) = TRUE;
		(get_content_from_card (new_card)->dirty) = TRUE;
	}

	UNLOCK_IPOD();

	return res;
#endif
	return GNOME_VFS_ERROR_NOT_SUPPORTED;
}


static GnomeVFSResult
do_truncate_handle (GnomeVFSMethod *method,
		    GnomeVFSMethodHandle *method_handle,
		    GnomeVFSFileSize where,
		    GnomeVFSContext *context)

{
	FileHandle *handle = (FileHandle *)method_handle;

	DEBUG_IPOD(("do_truncate_handle"));

	return gnome_vfs_truncate_handle (handle->handle, where);
}

static GnomeVFSResult
do_seek (GnomeVFSMethod *method,
		GnomeVFSMethodHandle *method_handle,
		GnomeVFSSeekPosition whence,
		GnomeVFSFileOffset offset,
		GnomeVFSContext *context)
{
	FileHandle *handle = (FileHandle *)method_handle;

	DEBUG_IPOD(("do_seek"));

	return gnome_vfs_seek (handle->handle, whence, offset);
}

static GnomeVFSResult
do_tell (GnomeVFSMethod *method,
		GnomeVFSMethodHandle *method_handle,
		GnomeVFSFileOffset *offset_return)
{
	FileHandle *handle = (FileHandle *)method_handle;

	DEBUG_IPOD(("do_tell"));

	return gnome_vfs_tell (handle->handle, offset_return);
}

static GnomeVFSResult
do_make_directory (GnomeVFSMethod *method,
		   GnomeVFSURI *uri,
		   guint perm,
		   GnomeVFSContext *context)
{
	char *dbpath, *name;
	GnomeVFSResult res;
	DEBUG_IPOD(("do_make_directory()\n"));

	LOCK_IPOD();
	res = update_database ();
	if (res != GNOME_VFS_OK)
	{
		UNLOCK_IPOD();
		return res;
	}
	UNLOCK_IPOD();

	/* o/~ We're in too deep o/~ */
	if (_gnome_vfs_uri_has_grand_parent (uri) == TRUE)
		return GNOME_VFS_ERROR_NOT_PERMITTED;

	dbpath = get_itunesdb_path ();
	name = _gnome_vfs_uri_get_basename (uri);

	// FIXME get the name, update the itunesdb if necessary
	LOCK_IPOD();
	iPodCreatePlaylist (itunesdb, dbpath, name);
	update_database ();
	UNLOCK_IPOD();

	g_free (dbpath);
	g_free (name);

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_remove_directory (GnomeVFSMethod *method,
		     GnomeVFSURI *uri,
		     GnomeVFSContext *context)
{
	DEBUG_IPOD(("do_remove_directory()"));

#if 0
	GnomeVFSResult res;
	int card, folder, song;
	GList *list;

	res = lookup_uri(uri, &card, &folder, &song);
	DEBUG_IPOD(("do_remove_directory(): lookup %d %d %d\n",
				card, folder, song));

	LOCK_IPOD();
	if (res != GNOME_VFS_OK)
	{
		UNLOCK_IPOD();
		return res;
	}

	if (folder == -1)
	{
		UNLOCK_IPOD();
		return GNOME_VFS_ERROR_ACCESS_DENIED;
	}

	if (song != -1)
	{
		UNLOCK_IPOD();
		return GNOME_VFS_ERROR_NOT_A_DIRECTORY;
	}

	rio->rio_hw = rio_new();
	if (rio_check(rio->rio_hw) == FALSE)
	{
		DEBUG_IPOD(("do_remove_directory: check of rio is FALSE\n"));
		UNLOCK_IPOD();
		return GNOME_VFS_ERROR_IO;
	}
	rio_set_card(rio->rio_hw, card);
	list = rio_get_content_from_card (card);
	/* Format the card when deleting the last directory */
	if (g_list_length (list) == 1)
		res  = rio_to_vfs_error(rio_format(rio->rio_hw));
	else
		res = rio_to_vfs_error(rio_del_folder(rio->rio_hw, folder));
	rio_delete(rio->rio_hw);

	if (res == GNOME_VFS_OK)
		(get_content_from_card (card)->dirty) = TRUE;

	UNLOCK_IPOD();

	return res;
#endif
	return GNOME_VFS_ERROR_NOT_SUPPORTED;
}

static GnomeVFSResult
do_set_file_info (GnomeVFSMethod *method,
		  GnomeVFSURI *uri,
		  const GnomeVFSFileInfo *info,
		  GnomeVFSSetFileInfoMask mask,
		  GnomeVFSContext *context)
{
	DEBUG_IPOD (("do_set_file_info: mask %d", mask));

#if 0
	GnomeVFSResult res = GNOME_VFS_OK;
	int card, folder, song;
	gboolean rename_folder = FALSE;
	const char *new_name;

	DEBUG_IPOD (("do_set_file_info: mask %d\n", mask));

	if (mask &
		(GNOME_VFS_SET_FILE_INFO_PERMISSIONS |
		GNOME_VFS_SET_FILE_INFO_OWNER |
		GNOME_VFS_SET_FILE_INFO_TIME))
			return GNOME_VFS_ERROR_NOT_SUPPORTED;

	if (mask & GNOME_VFS_SET_FILE_INFO_NAME) {
		new_name = gnome_vfs_unescape_string(info->name, "/");
		DEBUG_IPOD (("set_info: set new name: %s\n", new_name));

		res = lookup_uri(uri, &card, &folder, &song);
		DEBUG_IPOD(("do_set_file_info(): lookup %d %d %d\n",
					card, folder, song));

		LOCK_IPOD();
		if (res != GNOME_VFS_OK)
		{
			UNLOCK_IPOD();
			return res;
		}

		if (folder == -1)
		{
			UNLOCK_IPOD();
			return GNOME_VFS_ERROR_ACCESS_DENIED;
		}

		if (song == -1)
			rename_folder = TRUE;

		{
			GnomeVFSURI *new_uri;
			char *tmp, *new_filename;
			int new_card, new_folder, new_song;

			tmp = gnome_vfs_uri_extract_dirname(uri);
			DEBUG_IPOD(("do_set_file_info(): dirname %s\n",
						tmp));
			new_filename = g_strdup_printf("%s%s",
					tmp, new_name);
			DEBUG_IPOD(("do_set_file_info(): filename %s\n",
						new_filename));
			new_uri = gnome_vfs_uri_new(new_filename);
			
			UNLOCK_IPOD();
			res = lookup_uri(new_uri, &new_card, &new_folder,
					&new_song);
			LOCK_IPOD();

			g_free(new_filename);
			g_free(tmp);
			gnome_vfs_uri_unref(new_uri);
			
			if (res != GNOME_VFS_ERROR_NOT_FOUND)
			{
				UNLOCK_IPOD();
				return GNOME_VFS_ERROR_FILE_EXISTS;
			}
		}

		rio->rio_hw = rio_new();
		if (rio_check(rio->rio_hw) == FALSE)
		{
			DEBUG_IPOD(("do_set_file_info: check of rio is FALSE, destroying\n"));
			UNLOCK_IPOD();
			return GNOME_VFS_ERROR_IO;
		}
		rio_set_card(rio->rio_hw, card);

		if (rename_folder == TRUE)
		{
			res = rio_to_vfs_error(rio_rename_folder(rio->rio_hw,
						folder, (char *)new_name));
		} else {
			res = rio_to_vfs_error(rio_rename_song(rio->rio_hw,
						folder, song,
						(char *)new_name));
		}
		rio_delete(rio->rio_hw);

		if (res == GNOME_VFS_OK)
			(get_content_from_card (card)->dirty) = TRUE;

		UNLOCK_IPOD();
	}

	return res;
#endif
	return GNOME_VFS_ERROR_NOT_SUPPORTED;
}

static GnomeVFSResult
do_truncate (GnomeVFSMethod *method,
		GnomeVFSURI *uri,
		GnomeVFSFileSize where,
		GnomeVFSContext *context)
{
	GnomeVFSResult res;
	char *path;

	LOCK_IPOD ();
	path = find_path_for_file (uri, &res);
	UNLOCK_IPOD ();

	if (path == NULL)
	{
		if (res != GNOME_VFS_OK)
			return res;
		else
			return GNOME_VFS_ERROR_NOT_FOUND;
	}

	res = gnome_vfs_truncate (path, where);
	g_free (path);

	return res;
}

static GnomeVFSMethod method = {
	sizeof (GnomeVFSMethod),
	do_open,
	do_create,
	do_close,
	do_read,
	do_write,
	do_seek,
	do_tell,
	do_truncate_handle,
	do_open_directory,
	do_close_directory,
	do_read_directory,
	do_get_file_info,
	do_get_file_info_from_handle,
	do_is_local,
	do_make_directory,
	do_remove_directory,
	do_move,
	do_unlink,
	do_check_same_fs,
	do_set_file_info,
	do_truncate,
	NULL, /* do_find_directory */
	NULL, /* do_create_symbolic_link */
	NULL, /* do_monitor_add */
	NULL, /* do_monitor_cancel */
	NULL  /* do_file_control */
};

GnomeVFSMethod *
vfs_module_init (const char *method_name, const char *args)
{
	GConfClient *gc;

	ipod_lock = g_mutex_new();

	DEBUG_IPOD (("<-- ipod module init called -->"));

	gc = gconf_client_get_default ();
	mount_path = get_mount_path (gc);
	gconf_client_add_dir (gc, "/apps/qahog",
			GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
	gconf_client_notify_add (gc, GCONF_MOUNT_PATH,
			mount_path_changed_cb, NULL, NULL, NULL);

	return &method;
}

void
vfs_module_shutdown (GnomeVFSMethod *method)
{
	DEBUG_IPOD (("<-- ipod module shutdown called -->"));

	empty_database ();
	g_mutex_free(ipod_lock);
}

