
#include "config.h"

#include <id3tag.h>
#include <glib.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "iPod.h"
#include "mp3bitrate.h"

static char *
get_tag_utf8( struct id3_tag *tag, const char *field_name ) {
  unsigned int nstrings, j;
  const struct id3_frame *frame;
  const union id3_field *field;
  const id3_ucs4_t *ucs4;
  char * str = NULL;

  frame = id3_tag_findframe (tag, field_name, 0);
  if (frame == 0)
    return NULL;

  field = &frame->fields[1];
  nstrings = id3_field_getnstrings (field);
  for (j = 0; j < nstrings; j++)
  {
    ucs4 = id3_field_getstrings (field, j);

    if (strcmp (field_name, ID3_FRAME_GENRE) == 0)
      ucs4 = id3_genre_name (ucs4);

    str = id3_ucs4_utf8duplicate( ucs4 );
  }

  return str;
}

static void
add_dohm_to_tihm( tihm_t * tihm, int type, char * str ) {
  int i;
  Unicode * uni;

  i = tihm->num_dohm - 1;

  tihm->num_dohm += 1;
  tihm->dohms = g_realloc( tihm->dohms, tihm->num_dohm * sizeof( dohm_t ) );

  uni = utf8_to_unicode( str );

  tihm->dohms[i].type = type;
  tihm->dohms[i].size = uni->length;
  tihm->dohms[i].data = (u_int16_t *) uni->unicode;

  g_free( uni );
}

static int
id3_bitrate (FILE *iofile, int *bitrate, int *samplerate, int *time)
{
  long save_position;
  size_t length_read;
  guchar buffer[16384];
  int i, version, vbr, channels, found;

  *bitrate = 0;
  *samplerate = 0;
  *time = 0;
  channels = 0;
  version = 0;
  vbr = 0;
  found = 0;

  save_position = ftell( iofile );

  if ( fseek( iofile, 0, SEEK_SET) < 0 )
    return 0;

  length_read = fread( buffer, 1, sizeof( buffer ), iofile );

  if( length_read < 512 )
    goto bitdone;

  for( i = 0; i + 4 < length_read; i++)
  {
    if( mp3_bitrate_parse_header( buffer+i, length_read - i, bitrate, samplerate, time, &version, &vbr, &channels ) )
    {
      found = 1;
      break;
    }
  }

  /* If we haven't found anything, try again with 8 more kB */
  if (found == 0)
  {
    length_read = fread( buffer, 1, sizeof( buffer ), iofile );

    if (length_read < 512 )
      goto bitdone;

    for( i = 0; i + 4 < length_read; i++)
    {
      if( mp3_bitrate_parse_header( buffer+i, length_read - i, bitrate, samplerate, time, &version, &vbr, &channels ) )
      {
        break;
      }
    }
  }

bitdone:
  fseek( iofile, save_position, SEEK_SET );

  return 0;
}

static void
add_title_from_path( tihm_t * tihm, const char *path ) {
  char *tmp, *base, *suffix;
                                                                                
  tmp = g_strdup( path );
  suffix = strstr( tmp, ".mp3" );
  if ( suffix == NULL )
    suffix = strstr( tmp, ".MP3" );
  if ( suffix != NULL ) {
    *suffix = '\0';
  }
  base = strrchr( tmp, '/' ) + 1;
  add_dohm_to_tihm( tihm, IPOD_TITLE, base );
  g_free( tmp );
}

tihm_t * getMP3Info( const char * path ) {
  struct id3_file * file;
  struct id3_tag * tag;
  tihm_t * tihm;
  char * utf8 = NULL;
  struct stat s;
  int samplerate, bitrate, time;
  FILE *fd;
  int track_number = 0;

  /* We use some of our stuff right now */
  fd = fopen( path, "r" );
  g_return_val_if_fail( fd != NULL, NULL );
  id3_bitrate (fd, &bitrate, &samplerate, &time);
  fclose( fd );

  /* Now onto libid3tag */
  file = id3_file_open( path, ID3_FILE_MODE_READONLY );
  g_return_val_if_fail( file != NULL, NULL );

  /* Get the id3 tag */
  tag = id3_file_tag( file );
  if ( tag == NULL ) {
    id3_file_close( file );
    return NULL;
  }

  /* Create an tihm */
  tihm = g_new0( tihm_t, 1 );
 
  tihm->num_dohm = 1 ; /* One extra for the path */
  tihm->dohms = g_new0( dohm_t, 1 );

  /* Set all the id3 tags possible */

  /* If there's no tag, we'll make up something */
  if ( tag->nframes == 0 ) {
    add_title_from_path( tihm, path );
  } else {
    /* Title */
    utf8 = get_tag_utf8( tag, ID3_FRAME_TITLE );
    if ( utf8 != NULL ) {
      add_dohm_to_tihm( tihm, IPOD_TITLE, utf8 );
    } else {
      add_title_from_path( tihm, path );
    }
    g_free( utf8 );
    /* Artist */
    utf8 = get_tag_utf8( tag, ID3_FRAME_ARTIST );
    if ( utf8 != NULL )
      add_dohm_to_tihm( tihm, IPOD_ARTIST, utf8 );
    g_free( utf8 );
    /* Album */
    utf8 = get_tag_utf8( tag, ID3_FRAME_ALBUM );
    if ( utf8 != NULL )
      add_dohm_to_tihm( tihm, IPOD_ALBUM, utf8 );
    g_free( utf8 );
    /* Genre */
    utf8 = get_tag_utf8( tag, ID3_FRAME_GENRE );
    if ( utf8 != NULL )
      add_dohm_to_tihm( tihm, IPOD_GENRE, utf8 );
    g_free( utf8 );
    /* Comment */
    utf8 = get_tag_utf8( tag, ID3_FRAME_COMMENT );
    if ( utf8 != NULL )
      add_dohm_to_tihm( tihm, IPOD_COMMENT, utf8 );
    g_free( utf8 );
    /* Track number */
    utf8 = get_tag_utf8( tag, ID3_FRAME_TRACK );
    if ( utf8 != NULL ) {      
      track_number = atoi (utf8);
    }
    g_free( utf8 );
  }

  id3_file_close( file );

  tihm->dohms[tihm->num_dohm - 1].type = 0;
  tihm->dohms[tihm->num_dohm - 1].size = 0;

  /* Set mp3 information in tihm */
  stat( path, &s );
  /* See iTunesDB2/tihm.c tihm_db_fill () */
  tihm->type = 0x001;

  tihm->bitrate = bitrate;
  tihm->samplerate = samplerate;
  tihm->track = track_number;
  /* FIXME what is ->length used for ? */
  tihm->size = s.st_size;

  /* In ms */
  tihm->time = time * 1000;
  if (tihm->time == 0 && tihm->bitrate != 0) {
    tihm->time = ((double) tihm->size / 1000.0f) /
	    ((double) tihm->bitrate / 8.0f / 1000.0f) * 1000;
  }
  tihm->start_time = 0;
  tihm->stop_time = tihm->time;

  tihm->played_date = s.st_atime;
  tihm->mod_date = s.st_mtime;
  tihm->creation_date = s.st_ctime;

  /* All done... */
  return tihm;
}

void addMP3PathInfo( char * mount, tihm_t * tihm, char * path ) {
  char * new_path, * needle;
  Unicode * uni;

  g_return_if_fail( tihm != NULL );

  /* Found the start of the filename without the mount point */
  needle = g_strrstr( path, mount );
  g_return_if_fail( needle != NULL );
  needle = needle + strlen( mount );
  if (*needle != G_DIR_SEPARATOR)
    needle--;
  *needle = ':';

  /* Replace the G_DIR_SEPARATOR_S with colons MacOS-style */
  new_path = g_strdup( needle );
  needle = strstr( new_path, G_DIR_SEPARATOR_S );
  while ( needle != NULL ) {
    *needle = ':';
    needle = strstr( new_path, G_DIR_SEPARATOR_S );
  }

  uni = utf8_to_unicode( new_path );
  tihm->dohms[tihm->num_dohm - 1].type = IPOD_PATH;
  tihm->dohms[tihm->num_dohm - 1].size = uni->length;
  tihm->dohms[tihm->num_dohm - 1].data = (u_int16_t *) uni->unicode;

  g_free( new_path );
}

void addMP3PlayerInfo( tihm_t * tihm, int volume_adjustment,
		int times_played, int stars)
{
  g_return_if_fail( tihm != NULL );

  tihm->volume_adjustment = volume_adjustment;
  tihm->times_played = times_played;
  tihm->stars = stars;
}

