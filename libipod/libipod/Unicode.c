#include <glib.h>
#include <string.h>
#include "iPod.h"

char * unicode_to_utf8( Unicode * string ) {
  char *utf8_str;

  utf8_str = g_utf16_to_utf8( ( const gunichar2 * ) string->unicode,
		  string->length/2, NULL, NULL, NULL );

  return utf8_str;
}

Unicode * utf8_to_unicode( char *inbuf ) {
  Unicode *ret;
  long length;

  ret = g_new0 (Unicode, 1);

  ret->unicode = ( char * )g_utf8_to_utf16( ( const char * ) inbuf, -1,
		  &( length ), NULL, NULL);

  /* This is supposed to be the length in bytes
   * Making the assumption that a guint16 is twice the size of a char */
  ret->length = length * 2;

  return ret;
}

