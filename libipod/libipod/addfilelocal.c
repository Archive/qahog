#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include "iPod.h"

int main( int argc, char ** argv ) {
  tihm_t * tihm;
  itunesdb_t * iTunesDB;
  int res;

  if ( argc < 3 )
  {
	  g_print ("usage: %s <iTunesDB> <MP3>\n", argv[0]);
	  return 1;
  }

  iTunesDB = g_new0( itunesdb_t, 1 );
  if ( db_load( iTunesDB, argv[1] ) < 0 ) {
	  g_message ( "db_load failed" );
	  return 1;
  }

  tihm = getMP3Info( argv[2] );
  if( tihm == NULL ) {
	  g_message ( "getMP3Info failed" );
	  return 1;
  }

  if ( db_add( iTunesDB, tihm ) != 0 ) {
	  return 1;
  }

  res = db_write( *(iTunesDB), argv[1] );
  if ( res < 0 ) {
	  g_message( "db_write failed: %s", strerror( res ) );
	  return 1;
  }


  return 0;
}
