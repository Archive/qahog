#define _FILE_OFFSET_BITS 64
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>

#include <itunesdb.h>
#include "iPod.h"

/* define the modes for the directories */
#define IPOD_CONTROL_MODE 0755
#define ITUNES_MODE 0755

/* Determine if the iPod is actually openable by checking partition table */
/* Returns 0 if OK, -1 otherwise */
static int checkIfWinPod( char * device ) {
  int fd;
  char check[6];

  /* Open the iPod */
  fd = open( device, O_RDONLY );
  if ( fd == -1 ) {
    return -1;
  }
  
  /* Get the header */
  lseek( fd, 3, SEEK_SET );
  read( fd, check, 5 );
  check[5] = '\0';
  close( fd );

  /* perform comparison */
  if ( strcmp( check, "MSDOS" ) == 0 ) {
    return 0;
  }

  return -1;
}

/* Initialize the iPod directory structure */
static char * initPath( char * path, int level ) {
  int retval;

  /* Try creating iPod_Control */
  if ( level <= 1 ) {
    retval = mkdir( path, IPOD_CONTROL_MODE );
    if ( retval != 0 ) {
      free( path );
      return NULL;
    }
    strcat( path, "/iTunes" );
  }
  /* Try creating iTunes directory */
  if ( level <= 2 ) {
    retval = mkdir( path, ITUNES_MODE );
    if ( retval != 0 ) {
      free( path );
      return NULL;
    }
    strcat( path, "/iTunesDB" );
  }

  return path;
}

/* This function checks for the iTunesDB file at 
 * mountdir/iPod_Control/iTunes/iTunesDB */
char * checkiTunesDBPath( char * mountdir ) {
  char * path;
  int length; 

  /* Copy mount point onto path */
  path = (char *)malloc( 256 );
  length = strlen( mountdir );
  memcpy( path, mountdir, length );
  path[length] = '\0';
  
  /* Check for iPod_Control */
  strcat( path, "/iPod_Control" );
  if ( g_file_test( path, G_FILE_TEST_IS_DIR ) == FALSE ) {
    return initPath( path, 1 );
  }

  /* Check for iTunes */
  strcat( path, "/iTunes" );
  if ( g_file_test( path, G_FILE_TEST_IS_DIR ) == FALSE ) {
    return initPath( path, 2 );
  }

  return path;
}

/* Open the iTunesDB file */
static itunesdb_t * openiTunesDB( char * dbpath , char * name ) {
  itunesdb_t * iTunesDB;
  int ret;
   
  /* create the database */
  iTunesDB = g_new0( itunesdb_t, 1 );

  /* check to see if the file exists */
  if ( g_file_test( dbpath, G_FILE_TEST_IS_REGULAR ) == FALSE ) {
    g_print( "Couldn't open %s: %s\n", dbpath, strerror( errno ) );
    /* if not create a new one */
    if ( ( ret = db_create( iTunesDB, name, strlen( name ) ) ) != 0 ) {
      g_free( iTunesDB );
      return NULL;
    }
  }
  else {
    /* if so load it up */
    if ( db_load( iTunesDB, dbpath ) < 0 ) {
      g_print( "Couldn't load the database (%s)\n", dbpath );
      g_free( iTunesDB );
      return NULL;
    }
  }

  return iTunesDB;
}

static char * LoadEntireFile( char * file ) {
  char * buf;
  struct stat statb;
  int fd, file_size;

  if (stat (file, &statb) < 0) {
    return NULL;
  }

  file_size = statb.st_size;

  fd = open( file, O_RDONLY );
  if ( fd<0 )
    return NULL;
	      
  buf = g_malloc( file_size );
  if ( buf == NULL )
    return NULL;
		  
  file_size = read( fd, buf, file_size );
  close ( fd );

  return buf;
}

static char * GetMountFromFstab( char * device ) {
  char * contents, * needle, ** lines, * mount;
  int i, size;

  mount = NULL;

  contents = LoadEntireFile( "/etc/fstab" );
  if (contents == NULL)
    return NULL;

  lines = g_strsplit (contents, "\n", 0);
  g_free (contents);

  for (i = 0; lines[i] != NULL; i++) {
    char * ptr, *ptrend;

    /* Start by replacing everything after a # by spaces */
    needle = strstr( lines[i], "#" );
    while ( needle != NULL && *needle != '\0' )
    {
      *needle = ' ';
      needle++;
    }

    ptr = strstr( lines[i], device );
    if ( ptr == NULL )
      continue;

    /* Replace the tabs by spaces */
    needle = strstr( lines[i], "\t" );
    while ( needle != NULL )
    {
      *needle = ' ';
      needle = strstr( lines[i], "\t" );
    }

    /* We're pointing to the device name */
    ptr = strstr( ptr, " " );

    /* Pointing to the beginning of the mount point */
    ptr = strstr( ptr, "/" );
    if ( ptr == NULL )
      continue;

    /* Pointing to the space just after the mount point */
    ptrend = strstr( ptr, " " );
    if ( ptr == NULL )
      continue;

    size = ptrend - ptr;
    mount = g_malloc0( size + 1);
    mount = strncpy( mount, ptr, size );
    mount[size] = '\0';

    break;
  }

  g_strfreev( lines );

  return mount;
}

iPod_t * iPodOpenAutomatic( void ) {
  iPod_t * iPod;
  char i, * path;

  iPod = NULL;

  /* Cool, devfs is here */
  if ( g_file_test( "/dev/.devfsd", G_FILE_TEST_EXISTS ) == TRUE ) {
    i = 0;

    path = g_strdup_printf( "/dev/scsi/host0/bus0/target0/lun%d/disc", i );

    while ( g_file_test( path, G_FILE_TEST_EXISTS ) == TRUE
          && i < 4 && iPod == NULL ) {
      char * mount, * partition;

      partition = g_strdup_printf( "/dev/scsi/host0/bus0/target0/lun%d/part2",
          i );
      mount = GetMountFromFstab( partition );
      if ( mount != NULL )
        iPod = iPodOpen( path, mount );
      g_free( partition );

      g_free( path );
      g_free( mount );

      i++;
      path = g_strdup_printf( "/dev/scsi/host0/bus0/target0/lun%d/disc", i );
    }

    g_free( path );
  }

  if ( iPod != NULL )
    return iPod;

  i = 'a';
  path = g_strdup_printf( "/dev/sd%c", i );

  while ( g_file_test( path, G_FILE_TEST_EXISTS ) == TRUE
        && i < 'e' && iPod == NULL ) {
    char * mount, * partition;

    partition = g_strdup_printf( "/dev/sd%c2", i );
    mount = GetMountFromFstab( path );
    if ( mount != NULL )
      iPod = iPodOpen( path, mount );
    g_free( partition );

    g_free( path );
    g_free( mount );

    i++;
    path = g_strdup_printf( "/dev/sda%c", i );
  }

  g_free( path );

  return iPod;
}

/* This function will open the iPod device and return the iPod structure.
 * Returns NULL if fails 
 */
iPod_t * iPodOpen( char * device, char * mountdir ) {
  int returnval;
  iPod_t * iPod;
  char * command, * path, * name;

  /* Check if this device is a winpod */
  if ( checkIfWinPod( device ) != 0 ) {
    return NULL;
  }

  /* Mount the device */
  command = g_strdup_printf( "mount %s", mountdir );
  returnval = g_spawn_command_line_sync( command, NULL, NULL, NULL, NULL );
  if ( returnval == FALSE ) {
    g_free( command );
    perror( "HELP " );
    return NULL;
  }

  /* Build iPod structure */
  iPod = g_new0( iPod_t, 1 );
  iPod->mount = g_strdup( mountdir );
  iPod->name = NULL;

  /* find iTunesDB file */
  path = checkiTunesDBPath( mountdir );
  if ( path == NULL ) {
    fprintf( stderr, "Couldn't find the database\n");
    free( iPod );
    return NULL;
  }

  name = "iPod";
  iPod->dbpath = g_build_filename( G_DIR_SEPARATOR_S, path, "iPod", NULL );
  if ( g_file_test( iPod->dbpath, G_FILE_TEST_IS_REGULAR ) == FALSE )
  {
    g_free( iPod->dbpath );
    name = "iTunesDB";
    iPod->dbpath = g_build_filename( G_DIR_SEPARATOR_S, path,
        "iTunesDB", NULL );
  }

  /* find name of iPod */
 
  /* read iPod database in */
  iPod->iTunesDB = openiTunesDB( iPod->dbpath , name );
  if ( iPod->iTunesDB == NULL ) {
    fprintf( stderr, "Couldn't read the database\n");
    free( iPod );
    return NULL;
  }

  return iPod;
}

/* Free the iPod structure by freeing its pieces and then itself */
void iPodFree( iPod_t * iPod ) {
  free( iPod->mount );
  free( iPod->iTunesDB );
  free( iPod->dbpath );
  free( iPod->name );
  free( iPod );
}

/* Close the iPod by umounting it and free the structure */
int iPodClose( iPod_t * iPod ) {
  int retval = -1;
  char * command;

  if ( iPod != NULL ) {
    command = g_strdup_printf( "umount %s", iPod->mount );
    /* Destroy the structure so we can umount the iPod */
    iPodFree( iPod );

    retval = g_spawn_command_line_sync( command, NULL, NULL, NULL, NULL );
    g_free( command );
  }
  return retval;
}

