#include <stdio.h>
#include "iPod.h"

int main( int argc, char * argv[] ) {
  iPod_t * iPod;
  char * filename, * path;
  file_t * file;

  if ( argc < 2 )
  {
	  g_print ("usage: %s <file>", argv[0]);
	  return 1;
  }

//  iPod = iPodOpen( "/dev/sda", "/mnt/ipod" );
  iPod = iPodOpenAutomatic( );
  if ( iPod == NULL ) {
    printf( "UhOh\n" );
    iPodClose (iPod);
    return 1;
  }

  filename = g_path_get_basename( argv[1] );
  path = iPodMakePathName( iPod->mount, filename );

  g_print( "upload %s to %s\n", argv[1], path );

  file = iPodCopyFileTo( iPod, argv[1], path );

  if ( file == NULL )
    g_print( "Couldn't copy file to %s\n", path );
  else
    g_print( "Copied %s\n", path );

  iPodClose (iPod);

  return 0;
}
