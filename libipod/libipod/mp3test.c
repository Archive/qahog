
#include "iPod.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main( int argc, char * argv[] ) {
	tihm_t * info;

	if ( argc > 2 )
	{
		g_print( "usage: %s <file>", argv[0] );
		return 1;
	}

	info = getMP3Info( argv[1] );
	if ( info == NULL )
	{
		g_print( "No info for %s\n", argv[1] );
	} else {
		char *title, *artist, *genre;

		title = iPodGetSongField( info, IPOD_TITLE );
		artist = iPodGetSongField( info, IPOD_ARTIST );
		genre = iPodGetSongField( info, IPOD_GENRE );

		g_print ("Artist: %s\n", artist);
		g_print ("Title : %s\n", title);
		g_print ("Genre : %s\n", genre);
		g_print ("Bitrate: %d\n", info->bitrate);
		g_print ("Samplerate: %d\n", info->samplerate);
		g_print ("Length: %d sec or %d min %d sec\n", info->time / 1000, info->time / 1000 / 60, info->time / 1000 % 60);

		g_free (artist);
		g_free (title);
		g_free (genre);
	}

	return 0;
}

