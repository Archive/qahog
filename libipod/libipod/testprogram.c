#include <stdio.h>
#include "iPod.h"

#define print g_print
//#define print printf

int main() {
  iPod_t * iPod;
  GList *songs, *list;
  iPod = iPodOpen( "/dev/sda", "/mnt/ipod" );
  if ( iPod == NULL ) {
    printf( "UhOh\n" );
    iPodClose (iPod);
    return 1;
  }
  songs = db_song_list ( iPod->iTunesDB );
  list = songs;

  while (list != NULL) {
    tihm_t *song = list->data;
    char *title, *artist, *path, *genre;

    title = iPodGetSongField( song, IPOD_TITLE );
    artist = iPodGetSongField( song, IPOD_ARTIST );
    genre = iPodGetSongField( song, IPOD_GENRE );
    path = iPodGetSongRealPath( iPod, song );

    print ("Artist: %s\n", artist);
    print ("Title : %s\n", title);
    print ("Genre : %s\n", genre);
    print ("Path  : %s\n\n", path);

    g_free (artist);
    g_free (title);
    g_free (genre);
    g_free (path);

    list = list->next;
  }

  db_song_list_free( songs );
  iPodClose (iPod);
  return 0;
}
