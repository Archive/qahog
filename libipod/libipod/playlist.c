#include "iPod.h"
#include "itunesdb.h"

playlist_t * iPodCreatePlaylist( itunesdb_t * iTunesDB, const char * dbpath,
		char * name ) {
  Unicode * uniname;
  playlist_t * playlist;

  /* convert "name" to unicode */
  uniname = utf8_to_unicode( name );

  /* create the playlist on the existing iTunesDB */
  playlist = g_new0( playlist_t, 1 );
  playlist->name = name;
  playlist->num = db_playlist_create( iTunesDB, uniname->unicode, 
				      uniname->length );
  if ( playlist->num < 0 ) {
    g_free( uniname );
    g_free( playlist );
    return NULL;
  }
 
  /* Save the changes to the iTunesDB file and we are done */
  db_write( *iTunesDB, (char *)dbpath );
  return playlist;
}

/* Remove a playlist from the iPod */
int iPodRemovePlaylist( iPod_t * ipod, playlist_t * playlist ) {
  /* remove the playlist from the database */
  if ( db_playlist_delete( ipod->iTunesDB, playlist->num ) != 0 ) {
    return -1;
  }
  
  /* write out changes */
  if ( db_write( *(ipod->iTunesDB), ipod->dbpath ) != 0 ) {
    return -2;
  }
  
  return 0;
}

/* Add a song to a playlist on the iPod */
int iPodAddSongToPlaylist( iPod_t * ipod, playlist_t * playlist, 
			   file_t * song ) {
  /* add the song to the playlist */
  if ( db_playlist_tihm_add( ipod->iTunesDB, playlist->num, song->id ) != 0 ) {
    return -1;
  }
  
  /* write out all changes */
  if ( db_write( *(ipod->iTunesDB), ipod->dbpath ) != 0 ) {
    return -1;
  }
  
  return 0;
}

/* Remove a song from a playlist */
int iPodRemoveSongFromPlaylist( iPod_t * ipod, playlist_t * playlist,
				file_t * song ) {
  /* remove the song from the playlist */
  if( db_playlist_tihm_remove( ipod->iTunesDB, playlist->num, 
			       song->id ) != 0 ) {
    return -1;
  }

  /* If successful write out changes */
  if ( db_write( *(ipod->iTunesDB), ipod->dbpath ) != 0 ) {
    return -1;
  }
  
  return 0;
}

#if 0
/* Get list of playlists in database*/
static GList * iPodGetPlaylistList( iPod_t * ipod ) {
  /* get list of playlists */
  return db_playlist_list( ipod->iTunesDB );
}

/* Get list of songs from Playlist */
static GList * iPodGetPlaylistSongs( iPod_t * ipod, playlist_t * playlist ) {
  
//FIXME
  return NULL;
}
#endif


