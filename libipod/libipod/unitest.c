
#include "iPod.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main( int argc, char * argv[] ) {
	Unicode * uni;
	char * str;

	if ( argc > 2 )
	{
		g_print( "usage: %s <string>", argv[0] );
		return 1;
	}

	uni = utf8_to_unicode( argv[1] );
	str = unicode_to_utf8( uni );

	g_print( "argv: %s str: %s\n", argv[1], str );
	
	return 0;
}

