#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "iPod.h"

char * iPodMakePathName ( char *mount, char * name) {
  char * path, * dirname, * dirpath;
  int randnum, i;

  i = 0;

  randnum = ( int ) g_random_double_range( (gdouble) 0, ( gdouble) 100 );
  dirname = g_strdup_printf( "f%02d", randnum );
  path = g_build_filename( G_DIR_SEPARATOR_S, mount,
      "/iPod_Control/Music/", dirname, name, NULL );

  while ( ( g_file_test( path, G_FILE_TEST_IS_REGULAR) == TRUE )
        && i < 4 )
  {
    g_free( dirname );
    g_free( path );

    randnum = ( int ) g_random_double_range( (gdouble) 0, ( gdouble) 100 );
    dirname = g_strdup_printf( "f%02d", randnum );
    path = g_build_filename( G_DIR_SEPARATOR_S, mount,
        "/iPod_Control/Music/", dirname, name, NULL);

    i++;
  }

  if ( g_file_test( path, G_FILE_TEST_IS_REGULAR ) == TRUE )
  {
    g_free( path );
    g_free( dirname );
    return NULL;
  }

  dirpath = g_build_path( G_DIR_SEPARATOR_S, mount,
       "/iPod_Control/Music/", dirname, NULL );

  if ( g_file_test( dirpath, G_FILE_TEST_IS_DIR ) == FALSE )
  {
    if ( mkdir( dirpath, 0755 ) < 0 )
    {
      g_free( path );
      g_free( dirpath );
      g_free( dirname );
      return NULL;
    }
  }

  g_free( dirpath );
  g_free( dirname );

  return path;
}

static char *
makeFullPath( iPod_t * iPod, char * path ) {
  /* Create the full path to the file on the iPod */
  return g_build_filename( G_DIR_SEPARATOR_S, iPod->mount, path );
}

/* Make a directory on the iPod */
int iPodmkdir( iPod_t * iPod, char * path ) {
  char * fullpath;
  int retval;

  /* Make the full path */
  fullpath = makeFullPath( iPod, path );

  /* Call the mkdir function */
  retval = mkdir( fullpath, 0755 );
  g_free( fullpath );

  return retval;
}

/* Remove a directory on the iPod */
int iPodrmdir( iPod_t * iPod, char * path ) {
  char * fullpath;
  int retval;

  /* Make the full path */
  fullpath = makeFullPath( iPod, path );

  /* Call the mkdir function */
  retval = rmdir( fullpath );
  g_free( fullpath );

  return retval;
}

/* Remove a file on the iPod */
int iPodrm( iPod_t * iPod, file_t * file ) {
  char * fullpath;
  int retval;
  
  fullpath = makeFullPath( iPod, file->path );

  /* If file is an mp3 then remove from the database first */
  if ( file->id >= 0 ) {
    if ( db_remove( iPod->iTunesDB, file->id ) != 0 ) {
      return -1;
    }
    if ( db_write( *(iPod->iTunesDB), iPod->dbpath ) != 0 ) {
      return -2;
    }
  }
 
  retval = unlink( fullpath );
  g_free( fullpath );

  return retval;
}

/* Copy files (does a cp) */
static int CopyFiles( int infile, int outfile ) {
  char * data;
  int sizein, sizeout, written;

  written = 0;

  data = (char *) g_malloc( 32768 );

  /* copy the file */
  sizein = 0; sizeout = 0;
  sizein = read( infile, data, 32768 );
  while( sizein > 0 ) {
    while( written != sizein )
    {
      sizeout = write( outfile, data, sizein - written );
      written =+ sizeout;
    }
    sizein = read( infile, data, 32768 );
    written = 0;
  }

  if ( sizein < 0 )
    return -1;

  return 0;
}

/* Copy a file to the iPod */
file_t * iPodCopyFileTo ( iPod_t *iPod, char * from, char * to ) {
  int infile, outfile;
  int res;
  tihm_t * tihm;
  file_t * file;

  /* Make the file_t */
  file = g_new0( file_t, 1 );
  file->id = -1;

  if ( to == NULL )
  {
    char * filename;

    filename = g_path_get_basename( from );
    file->path = iPodMakePathName( iPod->mount, filename );
  } else {
    file->path = g_strdup( to );
  }

  /* Copy the files */
  infile = open( from, O_RDONLY );
  if ( infile <= 0 )
  {
    g_message( "couldn't open infile" );
    return NULL;
  }

  outfile = open( file->path, O_WRONLY | O_CREAT, 00755 );
  if ( outfile <= 0 )
  {
    close( infile );
    g_message( "couldn't open outfile" );
    return NULL;
  }

  if ( CopyFiles( infile, outfile ) != 0 ) {
    close( infile );
    close( outfile );
    g_free( file->path );
    g_free( file );
    g_message( "couldn't run CopyFiles" );
    return NULL;
  }
  /* We can close the outfile we are done with it */
  close( outfile );
  close( infile );

  /* get the tihm to add to the iTunesDB file */
  tihm = getMP3Info( to );
  if ( tihm == NULL ) {
    g_message( "getMP3Info failed" );
    return file;
  }

  addMP3PathInfo( iPod->mount, tihm, file->path );

  /* Update iTunesDB */
  if ( db_add( iPod->iTunesDB, tihm ) != 0 ) {
    g_free( file->path );
    g_free( file );
    g_message( "db_add failed" );
    return NULL;
  }
  file->id = tihm->num;
  res = db_write( *(iPod->iTunesDB), iPod->dbpath );
  if ( res < 0 ) {
    g_message( "db_write failed: %s", strerror( res ) );
    return NULL;
  }
  return file;
}

/* Copy a file from the iPod to somewhere else */
int iPodCopyFileFrom( iPod_t * iPod, file_t * from, char * to ) {
  char * fullpath;
  int infile, outfile;
  int ret;

  fullpath = makeFullPath( iPod, from->path );

  /* Copy the file */
  infile = open( fullpath, O_RDONLY );
  if ( infile <= 0 ) { return -1; }
  outfile = open( to, O_WRONLY | O_CREAT, 00755 );
  if ( outfile <= 0 ) { return -1; }
  ret = CopyFiles( infile, outfile );

  /* Close both files. We are done with them */
  close( infile );
  close( outfile );

  g_free( fullpath );

  /* Copy the files */
  return ret;
}

static file_t * getSongInfo( tihm_t * tihm ) {
  file_t * file;
  int cnt = 0;
  Unicode unicode;

  /* create the file type and get the num first */
  file = g_new0 ( file_t, 1 );
  file->id = tihm->num;

  unicode.length = 0;
  unicode.unicode = NULL;

  /* find the path associated with the song */
  while( cnt < tihm->num_dohm ) {
    dohm_t dohm = tihm->dohms[cnt];
    if ( dohm.type == IPOD_PATH ) {
      unicode.length = dohm.size;
      if ( unicode.length != 0 ) {
        unicode.unicode = g_malloc( dohm.size );
        memcpy( unicode.unicode, dohm.data, dohm.size );
      }
      break;
    }
    cnt++;
  }
  if ( unicode.length == 0 ) {
    return NULL;
  }
  /* convert unipath into a char array */
  file->path = unicode_to_utf8( &(unicode) );
  g_free( unicode.unicode );
  if ( file->path == NULL ) {
    return NULL;
  }
  return file;
}

GList * iPodGetSongList( iPod_t * iPod ) {
  GList * head = NULL, * look, * songs;
  file_t * cursong;
  /* Get songs from database */
  songs = db_song_list( iPod->iTunesDB );
  if ( songs == NULL ) {
    return NULL;
  }

  /* Convert songs into a format we can use */
  look = songs;
  while ( look != NULL ) {
    /* get the id for the song */
    cursong = getSongInfo( (tihm_t *)(look->data) );

    /* Put song onto chain */
    head = g_list_prepend( head, cursong );

    /* Goto the next song */
    look = songs->next;
    g_free( songs );
  }

  head = g_list_reverse( head );

  return head;
}

char * iPodGetSongField( tihm_t *song, int field )
{
  char * title = NULL;
  Unicode uni;
  int i;

  g_return_val_if_fail( song != NULL, NULL );
  g_return_val_if_fail( field > 0, NULL );

  uni.length = 0;
  uni.unicode = NULL;

  for ( i = 0 ; i < song->num_dohm ; i++ ) {
    if ( song->dohms[i].type == field ) {
      uni.length = song->dohms[i].size;
      if ( uni.length != 0 ) {
        uni.unicode = g_malloc( uni.length );
        memcpy( uni.unicode, song->dohms[i].data, song->dohms[i].size );
	title = unicode_to_utf8( &uni );
	g_free( uni.unicode );
      }

//      title = g_utf16_to_utf8( song->dohms[i].data, -1, NULL, NULL, NULL );
      break;
    }
  }

  return title;
}

char * iPodGetSongRealPathFromMount( const char *mount_path, tihm_t * song )
{
  char * path = NULL, * needle, * fullpath;

  g_return_val_if_fail (song != NULL, NULL);

  path = iPodGetSongField( song, IPOD_PATH );

  if (path == NULL)
    return NULL;

  needle = strstr (path, ":");
  while (needle != NULL) {
    *needle = G_DIR_SEPARATOR;
    needle = strstr (path, ":");
  }

  fullpath = g_build_filename( G_DIR_SEPARATOR_S, mount_path, path, NULL );
  g_free (path);

  return fullpath;
}

char * iPodGetSongRealPath( iPod_t * iPod, tihm_t * song )
{
	return iPodGetSongRealPathFromMount (iPod->mount, song);
}

