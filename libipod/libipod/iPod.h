#ifndef IPOD_H
#define IPOD_H

#include <itunesdb.h>
#include <glib.h>

/* Unicode stuff */
struct Unicode {
  /* it actually contains guint16/utf16 characters */
  char *unicode;
  /* length in bytes */
  int length;
};
typedef struct Unicode Unicode;

Unicode * utf8_to_unicode( char * inbuf );
char * unicode_to_utf8( Unicode * string );


/* iPod structures */
struct iPod_t {
  char * name;
  char * mount;
  char * dbpath;
  itunesdb_t * iTunesDB;
};
typedef struct iPod_t iPod_t;

/* Playlist type */
typedef pyhm_t playlist_t;

struct file_t {
  char * path;
  int id;
};
typedef struct file_t file_t;

/* iPod Functions */
iPod_t * iPodOpenAutomatic( void );
iPod_t * iPodOpen( char * device, char * mountdir );
void iPodFree( iPod_t * iPod );
int iPodClose( iPod_t * iPod );
char * checkiTunesDBPath( char * mountdir );

/* File functions files.c */
char * iPodMakePathName ( char * mount, char * name);
int iPodmkdir( iPod_t * iPod, char * path );
int iPodrmdir( iPod_t * iPod, char * path );
int iPodrm( iPod_t * iPod, file_t * file );
file_t * iPodCopyFileTo ( iPod_t *iPod, char * from, char * to );
int iPodCopyFileFrom( iPod_t * iPod, file_t * from, char * to );

/* Playlist functions */
/* returns a GList of file_t's */
GList * iPodGetSongList( iPod_t * iPod );
playlist_t * iPodCreatePlaylist( itunesdb_t * ipod, const char * dbpath,
		char * name );
int iPodRemovePlaylist( iPod_t * iPod, playlist_t * playlist );
int iPodAddSongToPlaylist( iPod_t * iPod, playlist_t * playlist, 
			   file_t * song );
int iPodRemoveSongFromPlaylist( iPod_t * iPod, playlist_t * playlist,
				file_t * song );

/* Helper functions to get some details about the song */
char * iPodGetSongField( tihm_t *song, int field );
char * iPodGetSongRealPath( iPod_t * iPod, tihm_t *song );
char * iPodGetSongRealPathFromMount( const char *mount_path, tihm_t * song );

/* mp3 stuff (mp3.c) */
tihm_t * getMP3Info( const char * path );
void addMP3PathInfo( char * mount, tihm_t * tihm, char * path );
void addMP3PlayerInfo( tihm_t * tihm, int volume_adjustment,
		int times_played, int stars);

#endif
