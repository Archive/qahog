#include "itunesdb.h"
#include "itunesdbi.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

static char *str_type(int file_type) {
  switch (file_type) {
  case IPOD_TITLE:
    return g_strdup( "Title" );
  case IPOD_PATH:
    return g_strdup( "Path" );
  case IPOD_ALBUM:
    return g_strdup( "Album" );
  case IPOD_ARTIST:
    return g_strdup( "Artist" );
  case IPOD_GENRE:
    return g_strdup( "Genre" );
  case IPOD_TYPE:
    return g_strdup( "Type" );
  case IPOD_COMMENT:
    return g_strdup( "Comment" );
  case IPOD_EQ:
    return g_strdup( "Equalizer");
  default:
    return g_strdup_printf( "Unknown (%d)", file_type );
  }
}

int main (int argc, char *argv[]) {
  GList *songs, *tmp;
  GList *playlists;

  tihm_t *tihm;
  pyhm_t *pyhm;
  char buffer[1024], *str;
  int i, ret;

  itunesdb_t itunesdb;

  if (db_load (&itunesdb, (char *) g_getenv ("ITUNESDB_PATH")) < 0) {
    if (db_load (&itunesdb, "/mnt/ipod/iPod_Control/iTunes/iTunesDB") < 0) {
      exit(1);
    }
  }

  printf ("Checking the sanity of the database...\n");

  ret = db_sanity_check(&itunesdb);

  if (ret <= -20) {
    printf ("There is something wrong with the loaded database!\n");
    db_free (&itunesdb);
    exit(1);
  } else if (ret < 0) {
    printf ("The database has small problems, continuing anyway. :: %i\n", ret);
  } else
    printf ("The database check out.\n");

  /* get song lists */
  songs = db_song_list (&itunesdb);

  if (songs == NULL)
    printf("Could not get song list\n");

  /* get playlists */
  playlists = db_playlist_list (&itunesdb);

  if (playlists == NULL) {
    printf("Could not get playlist list\n");
    db_free(&itunesdb);
    exit(1);
  }

  /* dump songlist contents */
  for (tmp = songs ; tmp ; tmp = tmp->next) {
    tihm = tmp->data;

    printf("%04i |\n", tihm->num);
    
    printf(" bitrate : %d\n", tihm->bitrate);
    printf(" length  : %d\n", tihm->length);
    printf(" size    : %d\n", tihm->size);
    printf(" type    : %d\n", tihm->type);
    printf(" num dohm: %d\n", tihm->num_dohm);
    printf(" samplert: %d\n", tihm->samplerate);
    printf(" stars   : %d\n", tihm->stars);
    printf(" played  : %d\n", tihm->times_played);
    printf(" \n");
    printf(" time     : %d\n", tihm->time);
    printf(" starttime: %d\n", tihm->start_time);
    printf(" stoptime : %d\n", tihm->stop_time);

    for (i = 0 ; i < tihm->num_dohm ; i++) {
      memset(buffer, 0, 1024);
      unicode_to_char (buffer, (char *)tihm->dohms[i].data, tihm->dohms[i].size);
      str = str_type(tihm->dohms[i].type);
      if (tihm->dohms[i].type == IPOD_EQ)
	printf (" %10s : %i\n", str, buffer[5]);
      else
	printf (" %10s : %s\n", str, buffer);

      g_free (str);
    }

    printf("\n");
  }

  /* dump playlist contents */
  for (tmp = playlists ; tmp ; tmp = tmp->next) {
    int num_ref;
    int *list = NULL;

    pyhm = (pyhm_t *)tmp->data;
    /* P(laylist) M(aster) */
    printf ("playlist name: %s(%s) len=%i\n", pyhm->name, (pyhm->num)?"P":"M", pyhm->name_len);

    num_ref = db_playlist_list_songs (&itunesdb, pyhm->num, &list);

    for (i = 0 ; i < num_ref ; i++)
      printf ("%i ", list[i]);

    printf ("\n");
    free(list);
  }

  db_free(&itunesdb);
  db_song_list_free (songs);
  db_playlist_list_free (playlists);

  return 0;
}
