#ifndef _iTunesDB_H
#define _iTunesDB_H
#include <itunesdb.h>

typedef itunesdb_t *dbhm;
#define mhdb dbhm
typedef tihm_t mp3info;

#if defined (USE_HFSPLUS)
itunesdb_t *openDB   (HFSPlus *volume, char *path);
int         closeDB (itunesdb_t *itunesdb, HFSPlus *volume, char *path);
#else
itunesdb_t *openDB   (char *path);
int         closeDB (itunesdb_t *itunesdb, char *path);
#endif

itunesdb_t *createDB (char *name, int length);


/* These function do not have the same arguments as the previous.
   Look at itunesdb.h to see what arguments to pass. */
#define addSong                db_add
#define createPlaylist         db_playlist_create
#define addSongtoPlaylist      db_playlist_tihm_add
#define removeSongfromPlaylist db_playlist_tihm_remove
#define removeSong             db_remove
#define removePlaylistfromDB   db_playlist_remove

#define freeDB  db_free

#endif
