/**
 *   (c) 2002 Nathan Hjelm <hjelmn@users.sourceforge.net>
 *   v0.1.2a v1compat.c
 *
 *   Contains functions to mimic how the previous version was called.
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the Lesser GNU Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *   
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *   
 *   You should have received a copy of the Lesser GNU Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 **/

#include "itunesdbi.h"
#include "iTunesDB.h"
#include <stdlib.h>
#include <stdio.h>

#if defined(USE_HFSPLUS)
itunesdb_t *openDB (HFSPlus *volume, char *path) {
#else
itunesdb_t *openDB (char *path) {
#endif
  itunesdb_t *itunesdb;

  itunesdb = (itunesdb_t *) malloc (sizeof (itunesdb_t));

  if (itunesdb == NULL) {
    perror ("openDB");
    return NULL;
  }

#if defined(USE_HFSPLUS)
  if (db_load (itunesdb, volume, path) < 0) {
#else
  if (db_load (itunesdb, path) < 0) {
#endif
    free (itunesdb);
    return NULL;
  }

  return itunesdb;
}

itunesdb_t *createDB (char *name, int length) {
  itunesdb_t *itunesdb;

  itunesdb = (itunesdb_t *) malloc (sizeof (itunesdb_t));

  if (itunesdb == NULL){
    perror("createDB");
    return NULL;
  }

  if (db_create (itunesdb, name, length) < 0) {
    free (itunesdb);
    return NULL;
  }

  return itunesdb;
}

#if defined(USE_HFSPLUS)
int closeDB (itunesdb_t *itunesdb, HFSPlus *volume, char *path) {
#else
int closeDB (itunesdb_t *itunesdb, char *path) {
#endif
  int ret;

#if defined(USE_HFSPLUS)
  ret = db_write (*itunesdb, volume, path);
#else
  ret = db_write (*itunesdb, path);
#endif

  db_free (itunesdb);

  /* free memory alloced by openDB.... just dont pass a local var in */
  free (itunesdb);

  return ret;
}
