#include <stdlib.h>
#include <string.h>
#include "itunesdb.h"

int main (int argc, char *argv[]) {
  ipod_t ipod;

  memset (&ipod, 0, sizeof(ipod_t));
  ipod_open (&ipod, "/mnt/ipod/", NULL, NULL);
  ipod_close (&ipod);

  return 0;
}
